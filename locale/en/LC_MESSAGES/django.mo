��    A      $  Y   ,      �  �   �  A   d  R   �  �   �  �   �  �   �  �  �	  �   :     �     �     �     �     �                    !     '     4     B     Q     U  
   Z     e     }     �  ^   �  B     B   D  D   �    �     �  
   �     �                .  )   ;  =   e     �     �     �     �  W   �     ;  ;   B  r   ~  $   �       �   )     �     �  B   �  "   %     H     ^  	   j  	   t  0   ~  N   �     �          	       B  9  �   |  A   O  R   �  �   �  �   �  �   �  �  �  �   %     �     �     �     �     �     �     �                         -     <     @  
   E     P     h     y  ^   �  B   �  B   /   D   r     �      �!  
   �!     �!     �!     "     "  )   &"  =   P"     �"     �"     �"     �"  W   �"     &#  ;   -#  r   i#  $   �#     $  �   $     �$     �$  B   �$  "   %     3%     I%  	   U%  	   _%  0   i%  N   �%     �%     �%     �%     
&            7          #   ,   5           $   )          6   =   
             	            3      -           @             &          (      :   .      <       !   1                 "      0         '                 %            8   ?       ;   *   4   >   A   2          +                  /                             9    
                                            Thanks again, <br/>
                                            <a href="https://isittheonion.com">isittheonion.com team</a>
                                         
                                Yo!
                             
        Seriously, <span class="domain"></span>? You should know better.
         
        The page <span class="domain"></span> has been marked as <a href="https://en.wikipedia.org/wiki/Clickbait" target="_blank">clickbait</a>.
            You should probably take their news with a pinch of salt.
         
        The page <span class="domain"></span> has been marked as <a href="https://en.wikipedia.org/wiki/Fake_news" target="_blank">fake news</a>.
            You should probably take their news with a pinch of salt.
         
        The page <span class="domain"></span> has been marked as <a href="https://en.wikipedia.org/wiki/News_satire" target="_blank">news satire</a>.
            Although funny, you should probably take their news with a pinch of salt.
         
        You're safe for now! <span class="domain"></span> is not on our list!
            But really, you should check their sources. A couple of helpful links:
            <ul>
<li><a href="http://www.fortliberty.org/hoax-sites.html">Fort Liberty</a></li>
<li><a href="http://p2t2solutions.com/tired-of-being-spoofed-by-fake-news-lies-rumors/">P2T2 Solutions</a></li>
<li><a href="http://LincolnReport.com">Lincoln Report</a></li>
<li><a href="http://www.newrepublic.com/article/118013/satire-news-websites-are-cashing-gullible-outraged-readers">New Republic</a></li>
<li><a href="http://rationalwiki.org/">Rational Wiki</a></li>
<li><a href="http://snopes.com">Snopes</a></li>
<li><a href="http://urbanlegends.about.com/">Urban Legends – About Entertainment</a></li>
</ul>
            If you find it should go on our list, <a data-toggle="modal" data-target="#report">
  let us know!
</a>
         <p>Thank you for your recent submission to isittheonion.com. We will review it as soon as possible.</p>
            <p>Your submission:</p> About Any comments? COOKIE_INFO_HEADER COOKIE_INFO_OK COOKIE_INFO_PARA Check Close Comment Email English (UK) Export to CSV Export to XLSX Hi! Home IP Address Invalid link: %(value)s Is it the Onion? It's NOT the onion! It's NOT the onion, and strictly speaking not fake, but should you <em>really</em> trust them? It's NOT the onion, but it is a different <em>clickbait</em> page! It's NOT the onion, but it is a different <em>fake news</em> page! It's NOT the onion, but it is a different <em>news satire</em> page! Lovingly made by <a href="https://twitter.com/thektokolwiek" target="_blank">Łukasz Kopeć</a>. Powered by <a href="https://www.djangoproject.com/" target="_blank">Django</a>.<br/>
            Disclaimer: This site is NOT affiliated with the Onion, the satirical news website. Name Nevermind! New submission from %(name)s! New submission: Offending URL Page address Page type {hit.diagnosis} at {hit.domain} Please confirm your email address by clicking the link below. Polish Recognised domain Recognised type of page Report website Sometimes the news are just too good to be true. Sometimes this is because they aren't! Submit Thank you for you submission to isittheonion.com, %(name)s! Thank you for your recent submission to isittheonion.com. We will review it as soon as possible.

Your submission: Thanks again,

isittheonion.com team The offending page This site lets you check against an array of known spoof websites. The list comes from <a href="http://www.fakenewswatch.com/" target="_blank">fakenewswatch.com</a>. Time submitted URL We also collect anonymous usage statistics to improve the service. Were you expecting something else? Yes, it IS the onion! Your E-mail Your Name [WayMore] [isittheonion.com] New submission from %(name)s! [isittheonion.com] Thank you for you submission to isittheonion.com, %(name)s! ^$ ^send/$ isittheonion.com team source: fakenewswatch.com Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-07-05 00:36+0000
PO-Revision-Date: 2016-07-05 01:37+0100
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: Łukasz <the.ktokolwiek@gmail.com>
Language-Team: 
X-Generator: Poedit 1.8.7
 
                                            Thanks again, <br/>
                                            <a href="https://isittheonion.com">isittheonion.com team</a>
                                         
                                Yo!
                             
        Seriously, <span class="domain"></span>? You should know better.
         
        The page <span class="domain"></span> has been marked as <a href="https://en.wikipedia.org/wiki/Clickbait" target="_blank">clickbait</a>.
            You should probably take their news with a pinch of salt.
         
        The page <span class="domain"></span> has been marked as <a href="https://en.wikipedia.org/wiki/Fake_news" target="_blank">fake news</a>.
            You should probably take their news with a pinch of salt.
         
        The page <span class="domain"></span> has been marked as <a href="https://en.wikipedia.org/wiki/News_satire" target="_blank">news satire</a>.
            Although funny, you should probably take their news with a pinch of salt.
         
        You're safe for now! <span class="domain"></span> is not on our list!
            But really, you should check their sources. A couple of helpful links:
            <ul>
<li><a href="http://www.fortliberty.org/hoax-sites.html">Fort Liberty</a></li>
<li><a href="http://p2t2solutions.com/tired-of-being-spoofed-by-fake-news-lies-rumors/">P2T2 Solutions</a></li>
<li><a href="http://LincolnReport.com">Lincoln Report</a></li>
<li><a href="http://www.newrepublic.com/article/118013/satire-news-websites-are-cashing-gullible-outraged-readers">New Republic</a></li>
<li><a href="http://rationalwiki.org/">Rational Wiki</a></li>
<li><a href="http://snopes.com">Snopes</a></li>
<li><a href="http://urbanlegends.about.com/">Urban Legends – About Entertainment</a></li>
</ul>
            If you find it should go on our list, <a data-toggle="modal" data-target="#report">
  let us know!
</a>
         <p>Thank you for your recent submission to isittheonion.com. We will review it as soon as possible.</p>
            <p>Your submission:</p> About Any comments? COOKIE_INFO_HEADER COOKIE_INFO_OK COOKIE_INFO_PARA Check Close Comment Email English (UK) Export to CSV Export to XLSX Hi! Home IP Address Invalid link: %(value)s Is it the Onion? It's NOT the onion! It's NOT the onion, and strictly speaking not fake, but should you <em>really</em> trust them? It's NOT the onion, but it is a different <em>clickbait</em> page! It's NOT the onion, but it is a different <em>fake news</em> page! It's NOT the onion, but it is a different <em>news satire</em> page! Lovingly made by <a href="https://twitter.com/thektokolwiek" target="_blank">Łukasz Kopeć</a>. Powered by <a href="https://www.djangoproject.com/" target="_blank">Django</a>.<br/>
            Disclaimer: This site is NOT affiliated with the Onion, the satirical news website. Name Nevermind! New submission from %(name)s! New submission: Offending URL Page address Page type {hit.diagnosis} at {hit.domain} Please confirm your email address by clicking the link below. polski Recognised domain Recognised type of page Report website Sometimes the news are just too good to be true. Sometimes this is because they aren't! Submit Thank you for you submission to isittheonion.com, %(name)s! Thank you for your recent submission to isittheonion.com. We will review it as soon as possible.

Your submission: Thanks again,

isittheonion.com team The offending page This site lets you check against an array of known spoof websites. The list comes from <a href="http://www.fakenewswatch.com/" target="_blank">fakenewswatch.com</a>. Time submitted URL We also collect anonymous usage statistics to improve the service. Were you expecting something else? Yes, it IS the onion! Your E-mail Your Name [WayMore] [isittheonion.com] New submission from %(name)s! [isittheonion.com] Thank you for you submission to isittheonion.com, %(name)s! ^$ ^send/$ isittheonion.com team source: fakenewswatch.com 