��    <      �  S   �      (  �   )  A   �  R   >  �   �  �   s  �   U  �  K	  �   �     ^     d     r     x     ~     �     �     �     �     �     �  
   �     �     �     �  ^     B   f  B   �  D   �    1     G  
   L     W     u     �     �  )   �     �     �     �     �  W   
     b  ;   i  r   �  $        =  �   P     �       B   	  "   L     o     �  	   �  0   �  N   �               &     <  �  V  �   �  A   �  T   �  �   :  �   #      �  2  �   �  	   �  	   �     �     �  	   �     �     �     �     �     �  	   �        #   	     -     D  A   [  :   �  H   �  C   !   &  e      �!  	   �!     �!     �!     �!     �!  +   �!     "     $"     6"     L"  \   \"     �"  9   �"  z   �"  #   v#     �#  �   �#     y$     �$  L   �$     �$     �$     %     %  0   ,%  L   ]%     �%     �%     �%     �%         5                 (       ;                     "                 4                    -                     '   %      /   1             0      !   <          *   7   9       #      6              &   8   $               3   +          .   )          ,          	   2       
            :        
                                            Thanks again, <br/>
                                            <a href="https://isittheonion.com">isittheonion.com team</a>
                                         
                                Yo!
                             
        Seriously, <span class="domain"></span>? You should know better.
         
        The page <span class="domain"></span> has been marked as <a href="https://en.wikipedia.org/wiki/Clickbait" target="_blank">clickbait</a>.
            You should probably take their news with a pinch of salt.
         
        The page <span class="domain"></span> has been marked as <a href="https://en.wikipedia.org/wiki/Fake_news" target="_blank">fake news</a>.
            You should probably take their news with a pinch of salt.
         
        The page <span class="domain"></span> has been marked as <a href="https://en.wikipedia.org/wiki/News_satire" target="_blank">news satire</a>.
            Although funny, you should probably take their news with a pinch of salt.
         
        You're safe for now! <span class="domain"></span> is not on our list!
            But really, you should check their sources. A couple of helpful links:
            <ul>
<li><a href="http://www.fortliberty.org/hoax-sites.html">Fort Liberty</a></li>
<li><a href="http://p2t2solutions.com/tired-of-being-spoofed-by-fake-news-lies-rumors/">P2T2 Solutions</a></li>
<li><a href="http://LincolnReport.com">Lincoln Report</a></li>
<li><a href="http://www.newrepublic.com/article/118013/satire-news-websites-are-cashing-gullible-outraged-readers">New Republic</a></li>
<li><a href="http://rationalwiki.org/">Rational Wiki</a></li>
<li><a href="http://snopes.com">Snopes</a></li>
<li><a href="http://urbanlegends.about.com/">Urban Legends – About Entertainment</a></li>
</ul>
            If you find it should go on our list, <a data-toggle="modal" data-target="#report">
  let us know!
</a>
         <p>Thank you for your recent submission to isittheonion.com. We will review it as soon as possible.</p>
            <p>Your submission:</p> About Any comments? Check Close Comment Email English (UK) Export to CSV Export to XLSX Hi! Home IP Address Invalid link: %(value)s Is it the Onion? It's NOT the onion! It's NOT the onion, and strictly speaking not fake, but should you <em>really</em> trust them? It's NOT the onion, but it is a different <em>clickbait</em> page! It's NOT the onion, but it is a different <em>fake news</em> page! It's NOT the onion, but it is a different <em>news satire</em> page! Lovingly made by <a href="https://twitter.com/thektokolwiek" target="_blank">Łukasz Kopeć</a>. Powered by <a href="https://www.djangoproject.com/" target="_blank">Django</a>.<br/>
            Disclaimer: This site is NOT affiliated with the Onion, the satirical news website. Name Nevermind! New submission from %(name)s! New submission: Offending URL Page address Page type {hit.diagnosis} at {hit.domain} Polish Recognised domain Recognised type of page Report website Sometimes the news are just too good to be true. Sometimes this is because they aren't! Submit Thank you for you submission to isittheonion.com, %(name)s! Thank you for your recent submission to isittheonion.com. We will review it as soon as possible.

Your submission: Thanks again,

isittheonion.com team The offending page This site lets you check against an array of known spoof websites. The list comes from <a href="http://www.fakenewswatch.com/" target="_blank">fakenewswatch.com</a>. Time submitted URL We also collect anonymous usage statistics to improve the service. Were you expecting something else? Yes, it IS the onion! Your E-mail Your Name [isittheonion.com] New submission from %(name)s! [isittheonion.com] Thank you for you submission to isittheonion.com, %(name)s! ^$ ^send/$ isittheonion.com team source: fakenewswatch.com Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-07-05 00:36+0000
PO-Revision-Date: 2016-07-05 02:13+0100
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Last-Translator: Łukasz <the.ktokolwiek@gmail.com>
Language-Team: 
X-Generator: Poedit 1.8.7
 
                                            Dzięki, <br/>
                                            <a href="https://isittheonion.com">isittheonion.com team</a> 
                                Yo!
                             
        Serio, <span class="domain"></span>? Pozwól, że nie skomentuję.
         
        Strona <span class="domain"></span> została oznaczona jako <a href="http://www.archibald.pl/clickbait/" target="_blank">clickbait</a>.
            Uważaj na jej treść, pewnie nie można jej traktować na serio.
         
        Strona <span class="domain"></span> została oznaczona jako <a href="https://en.wikipedia.org/wiki/Fake_news" target="_blank">zmyślone wiadomości</a>.
            Uważaj na jej treść, pewnie nie można jej traktować na serio.
         
        Strona <span class="domain"></span> została oznaczona jako <a href="https://en.wikipedia.org/wiki/News_satire" target="_blank">satyra wiadomości</a>.
            Mimo że jest śmieszna, uważaj na jej treść, bo pewnie nie można jej traktować na serio.
         
        Bezpieczeństwo! <span class="domain"></span> nie występuje na naszej liście!
            Ale naprawdę, sprawdzaj źródła. Tu jest kilka pomocnych odnośników:
            <ul>
<li><a href="http://www.fortliberty.org/hoax-sites.html">Fort Liberty</a></li>
<li><a href="http://p2t2solutions.com/tired-of-being-spoofed-by-fake-news-lies-rumors/">P2T2 Solutions</a></li>
<li><a href="http://LincolnReport.com">Lincoln Report</a></li>
<li><a href="http://www.newrepublic.com/article/118013/satire-news-websites-are-cashing-gullible-outraged-readers">New Republic</a></li>
<li><a href="http://rationalwiki.org/">Rational Wiki</a></li>
<li><a href="http://snopes.com">Snopes</a></li>
<li><a href="http://urbanlegends.about.com/">Urban Legends – About Entertainment</a></li>
</ul>
            Jeżeli uważasz, że ta strona powinna się jednak znaleźć na naszej liście, <a data-toggle="modal" data-target="#report">
  daj nam znać!
</a>
         <p>Dziękujemy za Twoje niedawne zgłoszenie dla isittheonion.com. Przejrzymy je kiedy to tylko możliwe.</p>
            <p>Twoje zgłoszenie:</p> O stronie Komentarz Sprawdź Zamknij Komentarz E-mail English (UK) Eksportuj do CSV Eksportuj do XLSX Cześć! Początek Adres IP Niepoprawny format linku: %(value)s Czy to jest the Onion? To NIE jest the Onion! To NIE jest the Onion, ale czy <em>na pewno</em> chcesz im ufać? To NIE jest the Onion, ale inna strona <em>clickbait</em>! To NIE jest the Onion, ale inna strona <em>zmyślonych wiadomości</em>! To NIE jest the Onion, ale inna strona <em>satyry wiadomości</em>! Autor: <a href="https://twitter.com/thektokolwiek" target="_blank">Łukasz Kopeć</a>. Powered by <a href="https://www.djangoproject.com/" target="_blank">Django</a>.<br/>
            Uwaga: Ta strona NIE jest w jakikolwiek sposób powiązana z the Onion, stroną ze zmyślonymi wiadomościami. Imię i nazwisko Wyczyść Nowa wiadomość od %(name)s! Nowa wiadomość: Adres strony Adres strony Strona typu {hit.diagnosis} na {hit.domain} polski Rozpoznana domena Rozpoznany typ strony Zgłoś stronę Czasami wiadomości są doprawdy niewiarygodne. Czasami to dlatego, że nie są godne wiary! Wyślij Dziękujemy za Twój wkład w isittheonion.com, %(name)s! Dziękujemy za Twoje niedawne zgłoszenie dla isittheonion.com. Przejrzymy je kiedy to tylko możliwe.

Twoje zgłoszenie: Dzięki,

drużyna isittheonion.com Adres strony Ta strona pozwala Ci sprawdzić, czy Twoje źródło jest na liście stron ze zmyślonymi wiadomościami. Lista pochodzi ze strony <a href="http://www.fakenewswatch.com/" target="_blank">fakenewswatch.com</a>. Czas kiedy wysłano URL Zbieramy też anonimowe statystyki użytkowania, w celu usprawnienia usług. Czego innego się spodziewać? Tak to JEST the Onion! E-mail Imię i nazwisko [isittheonion.com] Nowa wiadomość od %(name)s! [isittheonion.com] Dziękujemy za Twój wkład w isittheonion.com, %(name)s! ^$ ^send/$ drużyna isittheonion.com źródło: fakenewswatch.com 