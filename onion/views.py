# Create your views here.
from django.http import JsonResponse
from django.views.generic import FormView

from onion.forms import IsItForm, SendForm
from onion.utils import send_templated_mail


class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    """

    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'response': form.isit()
            }
            return JsonResponse(data)
        else:
            return response


class IndexView(AjaxableResponseMixin, FormView):
    template_name = 'onion/index.html'
    form_class = IsItForm
    success_url = '/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        return super(IndexView, self).form_valid(form)


class SendView(FormView):
    template_name = 'onion/send_modal.html'
    form_class = SendForm
    success_url = '/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        """
        Renders an e-mail to `email`.  `template_prefix` identifies the
        e-mail that is to be sent, e.g. "account/email/email_confirmation"
        """
        context = {'name': form.cleaned_data['name'],
                   'email': form.cleaned_data['email'],
                   'offending_url': form.cleaned_data['offending_uri'],
                   'comment': form.cleaned_data['comment'],
                   'request': self.request}
        send_templated_mail(template_prefix='onion/email/submit', from_email='default@isittheonion.com',
                            to_email='the.ktokolwiek@gmail.com', context=context)

        send_templated_mail(template_prefix='onion/email/submit_user', from_email='default@isittheonion.com',
                            to_email=context['email'], context=context)

        return super(SendView, self).form_valid(form)


send = SendView.as_view()

index = IndexView.as_view()
