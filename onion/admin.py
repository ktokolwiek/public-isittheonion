from django.contrib import admin

from onion.models import Hit
from onion.utils import export_as_csv_action, export_as_xlsx_action
from django.utils.translation import ugettext_lazy as _


class HitAdmin(admin.ModelAdmin):
    date_hierarchy = 'timestamp'
    list_display = ('timestamp', 'diagnosis', 'domain')
    list_filter = ('diagnosis', 'domain')
    actions = [export_as_csv_action(_("Export to CSV")),
               export_as_xlsx_action(_("Export to XLSX"))]

admin.site.register(Hit, HitAdmin)
