# -*- coding: utf-8 -*-
__author__ = 'lukasz'

from django.template import Library
from django.contrib.sites.models import Site
from django.conf import settings
from django.core.urlresolvers import resolve, reverse, NoReverseMatch
from django.utils.translation import activate, get_language, to_locale

register = Library()


@register.assignment_tag(takes_context=True)
def absolute_url(context, url=None, port=None, *args, **kwargs):
    """
    Get the url's full path including server
    Usage: {% absolute_url object.challenge.get_absolute_url as absolute_url %}
    """
    try:
        protocol = 'https' if context['request'].is_secure() else 'http'
    except KeyError:
        protocol = 'http'  # if we have no request
    current_site = Site.objects.get_current().domain
    # print(url)
    if getattr(settings, 'DEBUG', False):
        port = 8000
    if port:
        current_site = '%s:%s' % (current_site, port)
    # return context['request'].build_absolute_uri(url)
    return "%s://%s%s" % (protocol, current_site, url)