from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.test import TestCase, SimpleTestCase
from django.test import Client
from django.utils.translation import ugettext_lazy as _

class GetResponseTest(SimpleTestCase):

    def setUp(self):
        self.client = Client()


    def test_details(self):
        # Malformed URIs
        for uri in ['http://[c4']:
            response = self.client.post('/en/', {'uri': uri}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
            self.assertIsInstance(response, JsonResponse)
            self.assertContains(response, (_('Invalid link: %(value)s') % {'value': uri}), status_code=400)