from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.template import TemplateDoesNotExist
from django.template.loader import render_to_string
from inlinestyler.utils import inline_css


def send_templated_mail(template_prefix, from_email, to_email, context):
    subject = render_to_string('{0}_subject.txt'.format(template_prefix),
                               context)
    # remove superfluous line breaks
    subject = " ".join(subject.splitlines()).strip()

    bodies = {}
    for ext in ['html', 'txt']:
        try:
            template_name = '{0}_message.{1}'.format(template_prefix, ext)
            bodies[ext] = render_to_string(template_name,
                                           context).strip()
        except TemplateDoesNotExist:
            if ext == 'txt' and not bodies:
                # We need at least one body
                raise
    if 'txt' in bodies:
        msg = EmailMultiAlternatives(subject,
                                     bodies['txt'],
                                     from_email,
                                     [to_email])
        if 'html' in bodies:
            msg.attach_alternative(inline_css(bodies['html']), 'text/html')
    else:
        msg = EmailMessage(subject,
                           inline_css(bodies['html']),
                           from_email,
                           [to_email])
        msg.content_subtype = 'html'  # Main content is now text/html
    msg.send()


import csv
import openpyxl
from openpyxl.cell import get_column_letter
from django.http import HttpResponse

# from https://mousebender.wordpress.com/2006/11/10/recursive-getattrsetattr/
def rec_getattr(obj, attr):
    """Get object's attribute. May use dot notation.

    >>> class C(object): pass
    >>> a = C()
    >>> a.b = C()
    >>> a.b.c = 4
    >>> rec_getattr(a, 'b.c')
    4
    """
    if '.' not in attr:
        return getattr(obj, attr)
    else:
        L = attr.split('.')
        return rec_getattr(getattr(obj, L[0]), '.'.join(L[1:]))

def export_as_csv_action(description="Export selected objects as CSV file",
                         fields=None, exclude=None, header=True, extra_flds=None):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """
    def export_as_csv(modeladmin, request, queryset):
        """
        Generic csv export admin action.
        based on http://djangosnippets.org/snippets/1697/
        """
        opts = modeladmin.model._meta
        field_names = set([field.name for field in opts.fields])
        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset
        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset
        if extra_flds:
            field_names = field_names | set(extra_flds)

        response = HttpResponse(content_type='text/csv; charset=utf-8')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % ('%s' % opts).replace('.', '_')

        writer = csv.writer(response)
        if header:
            writer.writerow(list(field_names))
        for obj in queryset:
            writer.writerow([rec_getattr(obj, field) for field in field_names])
        return response
    export_as_csv.short_description = description
    return export_as_csv


def export_as_xlsx_action(description="Export selected objects as XLSX file",
                         fields=None, exclude=None, header=True, extra_flds=None):
    """
    This function returns an export xlsx action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """
    def export_as_xlsx(modeladmin, request, queryset):
        """
        Generic xlsx export admin action.
        based on http://djangosnippets.org/snippets/1697/
        """
        opts = modeladmin.model._meta
        field_names = set([field.name for field in opts.fields])
        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset
        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset
        if extra_flds:
            field_names = field_names | set(extra_flds)

        model_name = ('%s' % opts).replace('.', '_')

        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=%s.xlsx' % model_name

        wb = openpyxl.Workbook()
        ws = wb.get_active_sheet()
        ws.title = model_name

        row_num = 0

        columns = [(field, 30) for field in field_names]

        for col_num in range(len(columns)):
            c = ws.cell(row=row_num + 1, column=col_num + 1)
            c.value = '%s' % columns[col_num][0]
            c.style.font.bold = True
            # set column width
            ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]

        for obj in queryset:
            row_num += 1
            row = [rec_getattr(obj, field) for field in field_names]
            for col_num in range(len(row)):
                c = ws.cell(row=row_num + 1, column=col_num + 1)
                try:
                    c.value = row[col_num]
                except ValueError:
                    try:
                        c.value = '%.2f' % row[col_num]
                    except TypeError:
                        # just give up...
                        c.value = '%s' % row[col_num]
                c.style.alignment.wrap_text = True

        wb.save(response)
        return response

    export_as_xlsx.short_description = description
    return export_as_xlsx