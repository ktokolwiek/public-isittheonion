from urllib.parse import urlsplit

from django import forms
from django.core.exceptions import ValidationError
from django.db.models import TextField
from django.utils.translation import ugettext_lazy as _

from onion.models import Hit

list = {'fake': ['americannews.com',
                 'bigamericannews.com',
                 'twitter.com/capnews',
                 'christwire.org',
                 'civictribune.com',
                 'clickhole.com',
                 'creambmp.com',
                 'dcgazette.com',
                 'dailycurrant.com',
                 'dcclothesline.com',
                 'derfmagazine.com',
                 'drudgereport.com.co',
                 'duhprogressive.com',
                 'empirenews.com',
                 'enduringvision.com',
                 'www.cc.com/indecision',
                 'msnbc.co',
                 'msnbc.website',
                 'mediamass.net',
                 'nationalreport.net',
                 'newsbiscuit.com',
                 'news-hound.com',
                 'newsmutiny.com',
                 'politicalears.com',
                 'private-eye.co.uk',
                 'realnewsrightnow.com',
                 'rilenews.com',
                 'sprotspickle.com',
                 'thenewsnerd.com',
                 'theuspatriot.com',
                 'witscience.org', ],
        'satire': ['theonion.com',
                   'amplifyingglass.com',
                   'duffleblog.com',
                   'empiresports.co',
                   'gomerblog.com',
                   'huzlers.com',
                   'itaglive.com',
                   'newslo.com',
                   'thedailymash.co.uk',
                   'borowitzreport.com',
                   'dailybonnet.com',
                   'der-postillon.com',
                   'nahadaily.com',
                   'rockcitytimes.com',
                   'thelapine.ca',
                   'thespoof.com',
                   'weeklyworldnews.com',
                   'worldnewsdailyreport.com',
                   'aszdziennik.pl', ],
        'clickbait': ['21stcenturywire.com',
                      'activistpost.com',
                      'beforeitsnews.com',
                      'bigpzone.com',
                      'chronicle.su',
                      'coasttocoastam.com',
                      'consciouslifenews.com',
                      'conservativeoutfitters.com',
                      'wideawakeamerica.com',
                      'countdowntozerotime.com',
                      'counterpsyops.com',
                      'dailybuzzlive.com',
                      'disclose.tv',
                      'fprnradio.com',
                      'czystejelito.eu',
                      'geoengineeringwatch.org',
                      'globalresearch.ca',
                      'govtslaves.info',
                      'gulagbound.com',
                      'jonesreport.com',
                      'hangthebankers.com',
                      'humansarefree.com',
                      'infowars.com',
                      'intellihub.com',
                      'lewrockwell.com',
                      'libertytalk.fm',
                      'libertyvideos.org',
                      'libertymovementradio.com',
                      'megynkelly.us',
                      'naturalnews.com',
                      'newswire-24.com',
                      'nodisinfo.com',
                      'nowtheendbegins.com',
                      'pakalertpress.com',
                      'politicalblindspot.com',
                      'prisonplanet.com',
                      'prisonplanet.tv',
                      'realfarmacy.com',
                      'redflagnews.com',
                      'truthfrequencyradio.com',
                      'thedailysheeple.com',
                      'therundownlive.com',
                      'unconfirmedsources.com',
                      'veteranstoday.com',
                      'wakingupwisconsin.com',
                      'worldtruth.tv',
                      ],
        'stupid': ['dailymail.co.uk',
                   'fronda.pl']}


class IsItForm(forms.Form):
    uri = forms.CharField(widget=forms.TextInput(attrs={'size': 40}))

    def clean_uri(self):
        cleaned = self.cleaned_data['uri'].lower()
        try:
            split = urlsplit(cleaned).netloc
        except ValueError:
            raise ValidationError(
                _('Invalid link: %(value)s'),
                code='invalid',
                params={'value': cleaned},
            )
        if split:
            return cleaned
        else:
            split = urlsplit('http://' + cleaned).netloc
            if split:
                return 'http://' + cleaned
            else:
                raise ValidationError(
                    _('Invalid link: %(value)s'),
                    code='invalid',
                    params={'value': cleaned},
                )
                # return cleaned

    def isit(self):
        """
        Check if this website is in the domain which is listed

        :return: dict of type of site: bool (is it or not)
        """
        link = self.cleaned_data['uri']
        split = urlsplit(link).netloc
        response = {k: any(v in split for v in list[k]) for k in list.keys()}  # go through all possible lists above
        type_of_page = [k for k in response.keys() if response[k]]
        if not type_of_page:
            type_of_page = 'ok'
        else:
            type_of_page = type_of_page[0]
        response['onion'] = 'theonion.com' in split
        response['page'] = split
        response['type'] = type_of_page
        newhit = Hit(url=link, domain=split, diagnosis=type_of_page)
        newhit.save()
        return response


class SendForm(forms.Form):
    """
    Form for submitting new pages. Contains honeypot for spam prevention.
    """
    name = forms.CharField(label=_("Your Name"), widget=forms.TextInput(attrs={'size': 40}))
    email = forms.EmailField(label=_("Your E-mail"),)
    offending_uri = forms.CharField(label=_("The offending page"), widget=forms.TextInput(attrs={'size': 40}))
    comment = forms.CharField(label=_("Any comments?"), widget=forms.Textarea)
    huny = forms.HiddenInput()

