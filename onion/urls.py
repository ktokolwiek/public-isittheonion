from django.conf.urls import url
from django.conf.urls import patterns

from django.utils.translation import ugettext_lazy as _

app_name = 'onion'
urlpatterns = patterns('onion.views',
                       url(_(r'^$'), 'index', name='index'),
                       url(_(r'^send/$'), 'send', name='send'),
                       # url(_(r'^isit/$'), 'is_it', name='is_it'),
                       )