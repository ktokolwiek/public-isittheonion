from django.db import models

from django.utils.translation import ugettext_lazy as _

class Hit(models.Model):
    timestamp = models.DateTimeField(verbose_name=_("Time submitted"), auto_now=True)
    url = models.URLField(verbose_name=_('URL'))
    domain = models.TextField(verbose_name=_('Recognised domain'))
    diagnosis = models.TextField(verbose_name=_('Recognised type of page'))

    def __str__(self):
        return _('Page type {hit.diagnosis} at {hit.domain}').format(hit=self)